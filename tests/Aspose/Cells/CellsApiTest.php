<?php

require_once realpath(__DIR__) . '/Utils.php';

use Aspose\Cells\CellsApi;
use Aspose\Cells\AsposeApp;

class CellsApiTest extends PHPUnit_Framework_TestCase {
    
    protected $cells;

    protected function setUp()
    {    
        AsposeApp::$appSID = Utils::appSID;
        AsposeApp::$apiKey = Utils::apiKey;    
        $this->cells = new CellsApi();
    }
    
    public function testDeleteDecryptDocument()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $body = array("Password" => "123456");
        $result = $this->cells->DeleteDecryptDocument($name, $storage = null, $folder = null, $body);        
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteDocumentProperties()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_convert_cell.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteDocumentProperties($name, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteDocumentProperty()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_convert_cell.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteDocumentProperty($name, $propertyName="Author", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteDocumentUnProtectFromChanges()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_convert_cell.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteDocumentUnProtectFromChanges($name, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteUnProtectDocument()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_convert_cell.xlsx";
        Utils::uploadFile($name);

        $body = array("Password" => "123456");
        $result = $this->cells->DeleteUnProtectDocument($name, $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteUnprotectWorksheet()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_convert_cell.xlsx";
        Utils::uploadFile($name);

        $body = array("Password" => "123456");
        $result = $this->cells->DeleteUnprotectWorksheet($name, $sheetName="Sheet1", $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteWorkSheetBackground()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_convert_cell.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteWorkSheetBackground($name, $sheetName="Sheet1", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteWorkSheetComment()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteWorkSheetComment($name, $sheetName="Sheet1", $cellName="A2", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteWorkSheetHyperlink()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteWorkSheetHyperlink($name, $sheetName="Sheet3", $hyperlinkIndex="0", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteWorkSheetHyperlinks()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteWorkSheetHyperlinks($name, $sheetName="Sheet3", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteWorkSheetPictures()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteWorkSheetPictures($name, $sheetName="Sheet2", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteWorkSheetValidation()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteWorkSheetValidation($name, $sheetName="Sheet3", $validationIndex="0", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteWorksheet()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteWorksheet($name, $sheetName="Sheet3", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteWorksheetChartLegend()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteWorksheetChartLegend($name, $sheetName="Sheet1", $chartIndex="0", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteWorksheetChartTitle()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteWorksheetChartTitle($name, $sheetName="Sheet1", $chartIndex="0", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteWorksheetClearCharts()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteWorksheetClearCharts($name, $sheetName="Sheet1", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteWorksheetColumns()
    {
        $result = $this->cells->DeleteWorksheetColumns($name="test_cells.xlsx", $sheetName="Sheet1", $columnIndex="0", $columns="0", $updateReference="true", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteWorksheetDeleteChart()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteWorksheetDeleteChart($name, $sheetName="Sheet2", $chartIndex="0", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteWorksheetFreezePanes()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteWorksheetFreezePanes($name, $sheetName="Sheet3", $row=1, $column=1, $freezedRows=1, $freezedColumns=1, $folder = null, $storage = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteWorksheetOleObject()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteWorksheetOleObject($name, $sheetName="Sheet2", $oleObjectIndex="0", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteWorksheetOleObjects()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteWorksheetOleObjects($name, $sheetName="Sheet2", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteWorksheetPicture()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteWorksheetPicture($name, $sheetName="Sheet2", $pictureIndex="0", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteWorksheetPivotTable()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteWorksheetPivotTable($name, $sheetName="Sheet4", $pivotTableIndex="0", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteWorksheetPivotTables()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteWorksheetPivotTables($name, $sheetName="Sheet4", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteWorksheetRow()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteWorksheetRow($name, $sheetName="Sheet3", $rowIndex=1, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteWorksheetRows()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteWorksheetRows($name, $sheetName="Sheet3", $startrow=1, $totalRows=10, $updateReference = null, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetChartArea()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetChartArea($name, $sheetName="Sheet1", $chartIndex="0", $storage = null, $folder = null);        
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetChartAreaBorder()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetChartAreaBorder($name, $sheetName="Sheet1", $chartIndex="0", $storage = null, $folder = null);        
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetChartAreaFillFormat()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetChartAreaFillFormat($name, $sheetName="Sheet1", $chartIndex="0", $storage = null, $folder = null);        
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetDocumentProperties()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetDocumentProperties($name, $storage = null, $folder = null);        
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetDocumentProperty()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetDocumentProperty($name, $propertyName="Author", $storage = null, $folder = null);        
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetExtractBarcodes()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetExtractBarcodes($name, $sheetName="Sheet1", $pictureNumber="0", $storage = null, $folder = null);        
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorkBook()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorkBook($name, $password = null, $isAutoFit = null, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorkBookDefaultStyle()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorkBookDefaultStyle($name, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorkBookName()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorkBookName($name, $nameName="test_cells.xlsx", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorkBookNames()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorkBookNames($name, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorkBookTextItems()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorkBookTextItems($name, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorkBookWithFormat()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorkBookWithFormat($name, $format="pdf", $password = null, $isAutoFit = null, $storage = null, $folder = null, $outPath = null);
        $fh = fopen(realpath(__DIR__ . '/../../../../..') . '/Data/Output/Workbook.pdf', 'w');
        fwrite($fh, $result);
        fclose($fh);
        $this->assertFileExists(realpath(__DIR__ . '/../../../../..') . '/Data/Output/Workbook.pdf');
    }
    
    public function testGetWorkSheet()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorkSheet($name, $sheetName="Sheet1", $verticalResolution = null, $horizontalResolution = null, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorkSheetCalculateFormula()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorkSheetCalculateFormula($name, $sheetName="Sheet3", $formula="SUM(A3,A4)", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorkSheetComment()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorkSheetComment($name, $sheetName="Sheet1", $cellName="A2", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorkSheetComments()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorkSheetComments($name, $sheetName="Sheet1", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }

    public function testDeleteWorkSheetComments()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->DeleteWorkSheetComments($name, $sheetName="Sheet1", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }    
    
    public function testGetWorkSheetHyperlink()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorkSheetHyperlink($name, $sheetName="Sheet3", $hyperlinkIndex="0", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorkSheetHyperlinks()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorkSheetHyperlinks($name, $sheetName="Sheet3", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorkSheetMergedCell()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorkSheetMergedCell($name, $sheetName="Sheet3", $mergedCellIndex="0", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorkSheetMergedCells()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorkSheetMergedCells($name, $sheetName="Sheet3", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorkSheetTextItems()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorkSheetTextItems($name, $sheetName="Sheet3", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorkSheetValidation()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorkSheetValidation($name, $sheetName="Sheet1", $validationIndex="0", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorkSheetValidations()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorkSheetValidations($name, $sheetName="Sheet1", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorkSheetWithFormat()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorkSheetWithFormat($name, $sheetName="Sheet1", $format="png", $verticalResolution = null, $horizontalResolution = null, $storage = null, $folder = null);
        $fh = fopen(realpath(__DIR__ . '/../../../../..') . '/Data/Output/Sheet1.png', 'w');
        fwrite($fh, $result);
        fclose($fh);
        $this->assertFileExists(realpath(__DIR__ . '/../../../../..'). '/Data/Output/Sheet1.png');
    }
    
    public function testGetWorkSheets()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorkSheets($name, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorksheetAutoshape()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorksheetAutoshape($name, $sheetName="Sheet2", $autoshapeNumber=2, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorksheetAutoshapeWithFormat()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorksheetAutoshapeWithFormat($name, $sheetName="Sheet2", $autoshapeNumber=2, $format="png", $storage = null, $folder = null);
        $fh = fopen(realpath(__DIR__ . '/../../../../..'). '/Data/Output/Autoshape.png', 'w');
        fwrite($fh, $result);
        fclose($fh);
        $this->assertFileExists(realpath(__DIR__ . '/../../../../..'). '/Data/Output/Autoshape.png');
    }
    
    public function testGetWorksheetAutoshapes()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorksheetAutoshapes($name, $sheetName="Sheet2", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorksheetCell()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorksheetCell($name, $sheetName="Sheet1", $cellOrMethodName="A1", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorksheetCellStyle()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorksheetCellStyle($name, $sheetName="Sheet3", $cellName="A1", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorksheetCells()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorksheetCells($name, $sheetName="Sheet1", $offest = null, $count = null, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorksheetChart()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorksheetChart($name, $sheetName="Sheet1", $chartNumber="0", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorksheetChartLegend()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorksheetChartLegend($name, $sheetName="Sheet1", $chartIndex="0", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorksheetChartWithFormat()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorksheetChartWithFormat($name, $sheetName="Sheet1", $chartIndex="0", $format="png", $storage = null, $folder = null);
        $fh = fopen(realpath(__DIR__ . '/../../../../..'). '/Data/Output/Chart.png', 'w');
        fwrite($fh, $result);
        fclose($fh);
        $this->assertFileExists(realpath(__DIR__ . '/../../../../..'). '/Data/Output/Chart.png');
    }
    
    public function testGetWorksheetCharts()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorksheetCharts($name, $sheetName="Sheet1", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorksheetColumn()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorksheetColumn($name, $sheetName="Sheet1", $columnIndex=1, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorksheetColumns()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorksheetColumns($name, $sheetName="Sheet1", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorksheetOleObject()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorksheetOleObject($name, $sheetName="Sheet2", $objectNumber="0", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorksheetOleObjectWithFormat()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorksheetOleObjectWithFormat($name, $sheetName="Sheet2", $objectNumber="0", $format="png", $storage = null, $folder = null);
        $fh = fopen(realpath(__DIR__ . '/../../../../..'). '/Data/Output/Ole.png', 'w');
        fwrite($fh, $result);
        fclose($fh);
        $this->assertFileExists(realpath(__DIR__ . '/../../../../..'). '/Data/Output/Ole.png');
    }
    
    public function testGetWorksheetOleObjects()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorksheetOleObjects($name, $sheetName="Sheet2", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorksheetPicture()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorksheetPicture($name, $sheetName="Sheet2", $pictureNumber="0", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorksheetPictureWithFormat()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorksheetPictureWithFormat($name, $sheetName="Sheet2", $pictureNumber="0", $format="png", $storage = null, $folder = null);
        $fh = fopen(realpath(__DIR__ . '/../../../../..'). '/Data/Output/Picture.png', 'w');
        fwrite($fh, $result);
        fclose($fh);
        $this->assertFileExists(realpath(__DIR__ . '/../../../../..'). '/Data/Output/Picture.png');
    }
    
    public function testGetWorksheetPictures()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorksheetPictures($name, $sheetName="Sheet2", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorksheetPivotTable()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorksheetPivotTable($name, $sheetName="Sheet1", $pivottableIndex="0", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorksheetPivotTables()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorksheetPivotTables($name, $sheetName="Sheet4", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorksheetRow()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorksheetRow($name, $sheetName="Sheet2", $rowIndex="0", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorksheetRows()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->GetWorksheetRows($name, $sheetName="Sheet2", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostAutofitWorkbookRows()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $body = '{
                    "AutoFitMergedCells": true,
                    "IgnoreHidden": true,
                    "OnlyAuto": true
                  }';
        $result = $this->cells->PostAutofitWorkbookRows($name, $startRow = null, $endRow = null, $onlyAuto = null, $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostAutofitWorksheetRows()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $body = '{
                    "AutoFitMergedCells": true,
                    "IgnoreHidden": true,
                    "OnlyAuto": true
                  }';
        $result = $this->cells->PostAutofitWorksheetRows($name, $sheetName="Sheet3", $startRow = null, $endRow = null, $onlyAuto = null, $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostClearContents()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostClearContents($name, $sheetName="Sheet3", $range = "A1:C4", $startRow = null, $startColumn = null, $endRow = null, $endColumn = null, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostClearFormats()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostClearFormats($name, $sheetName="Sheet4", $range = "A1:G1", $startRow = null, $startColumn = null, $endRow = null, $endColumn = null, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostColumnStyle()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $body = '{
                    "Font": {
                      "Color": {
                        "A": "",
                        "R": "28",
                        "G": "24",
                        "B": "22"
                      },
                      "DoubleSize": 0,
                      "IsBold": true,
                      "IsItalic": true,
                      "IsStrikeout": true,
                      "IsSubscript": true,
                      "IsSuperscript": true,
                      "Name": "string",
                      "Size": 0,
                      "Underline": "string"
                    },
                    "Name": "Arial",
                    "CultureCustom": "string",
                    "Custom": "string",
                    "BackgroundColor": "SaaSpose.API.Business.Cells.DTO.Color",
                    "ForegroundColor": "SaaSpose.API.Business.Cells.DTO.Color",
                    "IsFormulaHidden": true,
                    "IsDateTime": true,
                    "IsTextWrapped": true,
                    "IsGradient": true,
                    "IsLocked": true,
                    "IsPercent": true,
                    "ShrinkToFit": true,
                    "IndentLevel": 0,
                    "Number": 0,
                    "RotationAngle": 0,
                    "Pattern": "string",
                    "TextDirection": "string",
                    "VerticalAlignment": "string",
                    "HorizontalAlignment": "string",
                    "BorderCollection": [
                      {
                        "LineStyle": "string",
                        "Color": "SaaSpose.API.Business.Cells.DTO.Color",
                        "BorderType": "string"
                      }
                    ],
                    "link": {
                      "Href": "string",
                      "Rel": "string",
                      "Type": "string",
                      "Title": "string"
                    }
                  }';
        $result = $this->cells->PostColumnStyle($name, $sheetName="Sheet3", $columnIndex=1, $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostCopyCellIntoCell()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostCopyCellIntoCell($name, $destCellName="A1", $sheetName="Sheet1", $worksheet="Sheet3", $cellname="A2", $row = null, $column = null, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostCopyWorksheet()
    {
        // Upload file to Aspose Cloud Storage
        $name = "Book1.xlsx";
        Utils::uploadFile($name);

        $body = '{
                    "CopyOptions": {
                        "CopyNames": "true"
                    }
                }';

        $result = $this->cells->PostCopyWorksheet($name, $sheetName="NewSheet", $sourceSheet="Sheet3", $folder = null, $storage = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostCopyWorksheetColumns()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostCopyWorksheetColumns($name, $sheetName="Sheet3", $sourceColumnIndex=2, $destinationColumnIndex=5, $columnNumber=6, $worksheet = null, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostCopyWorksheetRows()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostCopyWorksheetRows($name, $sheetName="Sheet3", $sourceRowIndex=3, $destinationRowIndex=7, $rowNumber=1, $worksheet = null, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostDocumentSaveAs()
    {
        // Upload file to Aspose Cloud Storage
        $name = "SAASCELLS-157-6.xltx";
        Utils::uploadFile($name);

        $body = '{
                    "CalculateFormula": true,
                    "CheckFontCompatibility": false,
                    "Compliance": "None",
                    "OnePagePerSheet": false,
                    "SaveFormat": "PDF"
                  }';
        $result = $this->cells->PostDocumentSaveAs($name, $newfilename = "SAASCELLS-157-6.pdf", $isAutoFitRows = null, $isAutoFitColumns = null, $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostEncryptDocument()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $body = array("EncriptionType"=>"XOR", "KeyLength"=>"128", "Password"=>"123456");
        $result = $this->cells->PostEncryptDocument($name, $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostGroupWorksheetColumns()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostGroupWorksheetColumns($name, $sheetName="Sheet3", $firstIndex=1, $lastIndex=5, $hide = null, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostGroupWorksheetRows()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostGroupWorksheetRows($name, $sheetName="Sheet3", $firstIndex=1, $lastIndex=5, $hide = null, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostHideWorksheetColumns()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostHideWorksheetColumns($name, $sheetName="Sheet3", $startColumn=1, $totalColumns=10, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostHideWorksheetRows()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostHideWorksheetRows($name, $sheetName="Sheet3", $startrow=1, $totalRows=10, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostImportDataMultipartContent()
    {
        // Upload file to Aspose Cloud Storage
        $name = "Book1.xlsx";
        Utils::uploadFile($name);

        $xmlData = "<ImportBatchDataOption>
                        <DestinationWorksheet>Sheet1</DestinationWorksheet>
                        <IsInsert>false</IsInsert>
                        <ImportDataType>BatchData</ImportDataType>
                        <Source>
                            <FileSourceType>RequestFiles</FileSourceType>
                            <FilePath>Batch_data_xml_2.txt</FilePath>
                        </Source>
                    </ImportBatchDataOption>";
        $file = realpath(__DIR__ . '/../../../../..') . '/Data/Batch_data_xml_2.txt';

        $result = $this->cells->PostImportDataMultipartContent($name, $storage = null, $folder = null, $xmlData, $file);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostImportDataCloudFileSystem()
    { 
        $options = array('{"BatchData":null,"DestinationWorksheet":"Sheet1","IsInsert":false,"ImportDataType":"BatchData","Source":{"FileSourceType":1,"FilePath":"Batch_data_json.txt"}}',
            '{"FirstRow":1,"FirstColumn":2,"IsVertical":true,"Data":null,"DestinationWorksheet":"Sheet1","IsInsert":true,"ImportDataType":"StringArray","Source":{"FileSourceType":1,"FilePath":"Array_string_json.txt"}}',
            '{"FirstRow":1,"FirstColumn":2,"IsVertical":true,"Data":null,"DestinationWorksheet":"Sheet1","IsInsert":true,"ImportDataType":"IntArray","Source":{"FileSourceType":1,"FilePath":"Array_int_json.txt"}}',
            '{"FirstRow":1,"FirstColumn":2,"IsVertical":true,"Data":null,"DestinationWorksheet":"Sheet1","IsInsert":true,"ImportDataType":"DoubleArray","Source":{"FileSourceType":1,"FilePath":"Array_double_json.txt"}}',
            '{"SeparatorString":",", "ConvertNumericData":true, "FirstRow":1, "FirstColumn":2, "SourceFile":"TestImportDataCSV.csv", "DestinationWorksheet":"Sheet1", "IsInsert":true, "ImportDataType":"CSVData", "Source":null}'
            );
        
        $dataFiles = array("Batch_data_json.txt", "Array_string_json.txt", "Array_int_json.txt", "Array_double_json.txt", "TestImportDataCSV.csv");

        foreach($options as $index=>$option) {
            // Upload file to Aspose Cloud Storage
            $name = "Book1.xlsx";
            Utils::uploadFile($name);
            $dataFile = $dataFiles[$index];
            Utils::uploadFile($dataFile);

            $body = $option;

            $result = $this->cells->PostImportDataCloudFile($name, $storage = null, $folder = null, $body);
            print "Response: $result";
            $this->assertEquals(200, $result->Code);
        }
    }

    public function testPostTaskDataMultipartContent()
    {   
        // Upload file to Aspose Cloud Storage
        $name = "TaskBook.xlsx";
        Utils::uploadFile($name);

        $xmlData = '{
                      "Tasks": [
                        {
                          "TaskType": 1,
                          "TaskParameter": {
                            "Workbook": {
                              "FileSourceType": 1,
                              "FilePath": "Book1.xlsx"
                            },
                            "ImportOption": {
                              "FirstRow": 1,
                              "FirstColumn": 1,
                              "IsVertical": false,
                              "Data": [
                                10.0,
                                1.222,
                                4.003
                              ],
                              "DestinationWorksheet": "Sheet1",
                              "IsInsert": true,
                              "ImportDataType": null,
                              "Source": null
                            },
                            "DestinationWorkbook": null
                          }
                        },
                        {
                          "TaskType": 4,
                          "TaskParameter": {
                            "ResultSource": 1,
                            "ResultDestination": {
                              "DestinationType": 1,
                              "InputFile": "Book1.xlsx",
                              "OutputFile": "Book2.xlsx"
                            }
                          }
                        }
                      ]
                    }';

        $file1 = realpath(__DIR__ . '/../../../../..') . '/Data/Batch_data_xml.txt';
        $file2 = realpath(__DIR__ . '/../../../../..') . '/Data/Batch_data_xml_2.txt';

        $files = array();
        $files[] = $file1;
        $files[] = $file2;

        $result = $this->cells->PostTaskDataMultipartContent($storage = null, $folder = null, $xmlData, $files);
        $this->assertNotNull($result);
    }

    public function testPostMoveWorksheet()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $body = '{
                    "DestinationWorksheet": "Sheet3",
                    "Position": "after"
                  }';
        $result = $this->cells->PostMoveWorksheet($name, $sheetName="Sheet1", $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostPivotTableCellStyle()
    {
        // Upload file to Aspose Cloud Storage
        $name = "sample.xlsx";
        Utils::uploadFile($name);

        $body = '{
                    "Style": {
                      "IsGradient": "true",
                      "Font": { "IsBold": "true" }
                    }
                  }';
        $result = $this->cells->PostPivotTableCellStyle($name, $sheetName="Sheet6", $pivotTableIndex="0", $column=1, $row=1, $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostPivotTableStyle()
    {
        // Upload file to Aspose Cloud Storage
        $name = "sample.xlsx";
        Utils::uploadFile($name);

        $body = '{
                    "Style": {
                      "IsGradient": "true",
                      "Font": { "IsBold": "true" }
                    }
                  }';
        $result = $this->cells->PostPivotTableStyle($name, $sheetName="Sheet6", $pivotTableIndex="0", $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostProtectDocument()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_convert_cell.xlsx";
        Utils::uploadFile($name);

        $body = array("ProtectionType"=>"All", "Password"=>"abc");
        $result = $this->cells->PostProtectDocument($name, $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostRenameWorksheet()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostRenameWorksheet($name="test_cells.xlsx", $sheetName="Sheet4", $newname="RenameSheet4", $folder = null, $storage = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostRowStyle()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $body = '{
                "Font": {
                  "Color": {
                    "A": "",
                    "R": "44",
                    "G": "24",
                    "B": "22"
                  },
                  "DoubleSize": 0,
                  "IsBold": true,
                  "IsItalic": true,
                  "IsStrikeout": true,
                  "IsSubscript": true,
                  "IsSuperscript": true,
                  "Name": "string",
                  "Size": 0,
                  "Underline": "string"
                },
                "Name": "Arial",
                "CultureCustom": "string",
                "Custom": "string",
                "BackgroundColor": "SaaSpose.API.Business.Cells.DTO.Color",
                "ForegroundColor": "SaaSpose.API.Business.Cells.DTO.Color",
                "IsFormulaHidden": true,
                "IsDateTime": true,
                "IsTextWrapped": true,
                "IsGradient": true,
                "IsLocked": true,
                "IsPercent": true,
                "ShrinkToFit": true,
                "IndentLevel": 0,
                "Number": 0,
                "RotationAngle": 0,
                "Pattern": "string",
                "TextDirection": "string",
                "VerticalAlignment": "string",
                "HorizontalAlignment": "string",
                "BorderCollection": [
                  {
                    "LineStyle": "string",
                    "Color": "SaaSpose.API.Business.Cells.DTO.Color",
                    "BorderType": "string"
                  }
                ],
                "link": {
                  "Href": "string",
                  "Rel": "string",
                  "Type": "string",
                  "Title": "string"
                }
              }';
        $result = $this->cells->PostRowStyle($name, $sheetName="Sheet3", $rowIndex=1, $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostSetCellHtmlString()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $file = getcwd() . '/Data/Input/index.html';
        $result = $this->cells->PostSetCellHtmlString($name, $sheetName="Sheet3", $cellName="A1", $storage = null, $folder = null, $file);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostSetCellRangeValue()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostSetCellRangeValue($name, $sheetName="Sheet3", $cellarea="A1:A5", $value="sample", $type="string", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostSetWorksheetColumnWidth()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostSetWorksheetColumnWidth($name, $sheetName="Sheet3", $columnIndex=1, $width=200, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostUngroupWorksheetColumns()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostUngroupWorksheetColumns($name, $sheetName="Sheet3", $firstIndex=1, $lastIndex=5, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostUngroupWorksheetRows()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostUngroupWorksheetRows($name, $sheetName="Sheet3", $firstIndex=1, $lastIndex=5, $isAll = null, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostUnhideWorksheetColumns()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostUnhideWorksheetColumns($name, $sheetName="Sheet3", $startcolumn=1, $totalColumns=5, $width = null, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostUnhideWorksheetRows()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostUnhideWorksheetRows($name, $sheetName="Sheet3", $startrow=1, $totalRows=5, $height = null, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostUpdateWorksheetCellStyle()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $body = '{
                    "BackgroundThemeColor": {
                        "ColorType": "Text2",
                        "Tint": 1
                    }
                }';
        $result = $this->cells->PostUpdateWorksheetCellStyle($name, $sheetName="Sheet3", $cellName="A1", $storage = null, $folder = null, $body);
        //echo json_encode($result, JSON_PRETTY_PRINT);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostUpdateWorksheetOleObject()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $body = '{
                    "OleObject": {
                      "Name" : "ole",
                      "UpperLeftRow" : "18",
                      "Top" : "100",
                      "UpperLeftColumn" : "18",
                      "Left" : "100",
                      "LowerRightRow" : "24",
                      "Bottom" : "0",
                      "LowerRightColumn" : "2",
                      "Right" : "0",
                      "Width" : "100",
                      "Height" : "100",
                      "X" : "64",
                      "Y" : "360",
                      "DisplayAsIcon" : "false",
                      "FileType" : "Unknown",
                      "IsAutoSize" : "false",
                      "IsLink" : "false",
                      "SourceFullName" : "ole.docx",
                      "ImageSourceFullName" : "WaterMark.png",
                    }}';
        $result = $this->cells->PostUpdateWorksheetOleObject($name, $sheetName="Sheet2", $oleObjectIndex="0", $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostUpdateWorksheetProperty()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $body = array("Type"=>"Worksheet", "Name"=>"NewSheet", "IsGridlinesVisible"=>"false");
        $result = $this->cells->PostUpdateWorksheetProperty($name, $sheetName="Sheet3", $folder = null, $storage = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostUpdateWorksheetRangeStyle()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $body = '{
                  "Font": {
                    "IsBold": "true"
                  },
                  "BackgroundColor": {
                    "A": 255,
                    "R": 255,
                    "G": 0,
                    "B": 0
                  },
                  "ForegroundColor": {
                    "A": 255,
                    "R": 0,
                    "G": 0,
                    "B": 255
                  },
                  "Pattern" : "VerticalStripe"
                }';
        $result = $this->cells->PostUpdateWorksheetRangeStyle($name, $sheetName="Sheet3", $range="A1:G10", $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostUpdateWorksheetRow()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostUpdateWorksheetRow($name, $sheetName="Sheet3", $rowIndex=1, $height=50, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostWorkSheetComment()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $body = '{
                    "CellName": "A2",
                    "Author": "Masood",
                    "HtmlNote": "",
                    "Note": "Update comments",
                    "AutoSize": true,
                    "IsVisible": true,
                    "Width": 0,
                    "Height": 0,
                    "TextHorizontalAlignment": "",
                    "TextOrientationType": "",
                    "TextVerticalAlignment": "",
                    "link": {
                      "Href": "",
                      "Rel": "",
                      "Type": "",
                      "Title": ""
                    }
                  }';
        $result = $this->cells->PostWorkSheetComment($name, $sheetName="Sheet1", $cellName="A2", $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostWorkSheetHyperlink()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $body = array("Address"=>"http://www.aspose.com", "TextToDisplay"=>"Welcome to Aspose", "ScreenTip"=>"Hello World");
        $result = $this->cells->PostWorkSheetHyperlink($name, $sheetName="Sheet3", $hyperlinkIndex="0", $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostWorkSheetPicture()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $body = array("Name"=>"test_picture", "AutoShapeType"=>"Picture", "Placement"=>"MoveAndSize",
                      "UpperLeftRow"=>5, "Top"=>100, "UpperLeftColumn"=>5, "Left"=>100);
        $result = $this->cells->PostWorkSheetPicture($name, $sheetName="Sheet2", $pictureIndex="0", $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostWorkSheetTextSearch()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostWorkSheetTextSearch($name, $sheetName="Sheet2", $text="OLE", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostWorkSheetValidation()
    {
        $result = $this->cells->PostWorkSheetValidation($name="test_cells.xlsx", $sheetName="Sheet2", $validationIndex, $storage = null, $folder = null, $file);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostWorkbookCalculateFormula()
    {
        // Upload file to Aspose Cloud Storage
        $name = "Book1.xlsx";
        Utils::uploadFile($name);

        $options = '{
                        "CalcStackSize": "1"
                    }';
        $ignore_error = "true";

        $result = $this->cells->postWorkbookCalculateFormula($name, $options, $ignore_error, $storage=null, $folder=null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostWorkbookGetSmartMarkerResult()
    {
        // Upload file to Aspose Cloud Storage
        $name = "Book1.xlsx";
        Utils::uploadFile($name);

        $file = realpath(__DIR__ . '/../../../../..') . '/Data/Input/Marker.xml';
        $result = $this->cells->PostWorkbookGetSmartMarkerResult($name="Book1.xlsx", $xmlFile = null, $storage = null, $folder = null, $outPath = "smartmarker1.xlsx", $file);
        $this->assertEquals(200, $result->StatusCode);
    }
    
    public function testPostWorkbookSplit()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostWorkbookSplit($name, $format = "png", $from = null, $to = null, $horizontalResolution = null, $verticalResolution = null, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostWorkbooksMerge()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $mergeWith = "test_convert_cell.xlsx";
        Utils::uploadFile($mergeWith);

        $result = $this->cells->PostWorkbooksMerge($name, $mergeWith, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostWorkbooksTextReplace()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostWorkbooksTextReplace($name, $oldValue="OLE", $newValue="OleObject", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostWorkbooksTextSearch()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostWorkbooksTextSearch($name, $text="OLE", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostWorksheetCellSetValue()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostWorksheetCellSetValue($name, $sheetName="Sheet3", $cellName="A1", $value="HelloWorld", $type="string", $formula = null, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostWorksheetChartLegend()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $body = array("Legend"=>array("Position"=>"Bottom"));
        $result = $this->cells->PostWorksheetChartLegend($name, $sheetName="Sheet1", $chartIndex="0", $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostWorksheetChartTitle()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $body = array("Title"=>array("Text"=>"Sales Report"));
        $result = $this->cells->PostWorksheetChartTitle($name, $sheetName="Sheet1", $chartIndex="0", $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostWorksheetMerge()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostWorksheetMerge($name, $sheetName="Sheet1", $startRow=1, $startColumn=1, $totalRows=10, $totalColumns=10, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostWorksheetRangeSort()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $body = array(
                    "CaseSensitive" => true,
                    "HasHeaders" => true,
                    'SortLeftToRight'=> false,
                    "KeyList" => array(
                      array(
                        "Key"=> 4,
                        "SortOrder"=> "descending"
                      ),
                      array(
                        "Key"=> 5,
                        "SortOrder"=> "descending"
                      )
                    )
                  );
        $result = $this->cells->PostWorksheetRangeSort($name, $sheetName="Sheet4", $cellArea="A1:G22", $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostWorksheetUnmerge()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostWorksheetUnmerge($name, $sheetName="Sheet3", $startRow=1, $startColumn=1, $totalRows=10, $totalColumns=10, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPostWorsheetTextReplace()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PostWorsheetTextReplace($name, $sheetName="Sheet2", $oldValue="OLE", $newValue="Ole", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPutAddNewWorksheet()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PutAddNewWorksheet($name, $sheetName="NewSheet", $storage = null, $folder = null);
        $this->assertEquals('Created', $result->Status);
    }
    
    public function testPutChangeVisibilityWorksheet()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PutChangeVisibilityWorksheet($name, $sheetName="Sheet1", $isVisible="true", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPutConvertWorkBook()
    {
        $file = realpath(__DIR__ . '/../../../../..') . '/Data/Book1.xlsx';
        $result = $this->cells->PutConvertWorkBook($format = "pdf", $password = null, $outPath = null, $file);
        

        $fh = fopen(realpath(__DIR__ . '/../../../../..'). '/Data/Book1_out.pdf', 'w');
        fwrite($fh, $result);
        fclose($fh);
        $this->assertFileExists(realpath(__DIR__ . '/../../../../..'). '/Data/Book1_out.pdf');
    }
    
    public function testPutConvertWorkBookMultipart()
    {
        $file = realpath(__DIR__ . '/../../../../..') . '/Data/Book1.xlsx';
        $jsonData = '{"CalculateFormula": true,"CheckFontCompatibility": false,"Compliance": "None","OnePagePerSheet": false,"SaveFormat": "PDF"}';
        $result = $this->cells->PutConvertWorkBookMultipart($format = "pdf", $password = null, $outPath = "Book1.pdf", $jsonData, $file);
    }

    public function testPutDocumentProperty()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $body = array("Value"=>"Aspose");
        $result = $this->cells->PutDocumentProperty($name, $propertyName="Title", $storage = null, $folder = null, $body);
        $this->assertEquals('Created', $result->Status);
    }
    
    public function testPutDocumentProtectFromChanges()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $body = array("ProtectionType"=>"all", "Password"=>"123456");
        $result = $this->cells->PutDocumentProtectFromChanges($name, $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPutInsertWorksheetColumns()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PutInsertWorksheetColumns($name, $sheetName="Sheet1", $columnIndex=1, $columns=5, $updateReference = null, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPutInsertWorksheetRow()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PutInsertWorksheetRow($name, $sheetName="Sheet1", $rowIndex=1, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPutInsertWorksheetRows()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PutInsertWorksheetRows($name, $sheetName="Sheet1", $startrow=1, $totalRows=10, $updateReference = null, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPutWorkSheetBackground()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $file = realpath(__DIR__ . '/../../../../..') . '/Data/Input/watermark.png';
        $result = $this->cells->PutWorkSheetBackground($name, $sheetName="Sheet1", $storage = null, $folder = null, $file);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPutWorkSheetComment()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $body = '{
                    "CellName": "A1",
                    "Author": "Masood",
                    "HtmlNote": "",
                    "Note": "Add comments",
                    "AutoSize": true,
                    "IsVisible": true,
                    "Width": 0,
                    "Height": 0,
                    "TextHorizontalAlignment": "",
                    "TextOrientationType": "",
                    "TextVerticalAlignment": "",
                    "link": {
                      "Href": "",
                      "Rel": "",
                      "Type": "",
                      "Title": ""
                    }
                  }';
        $result = $this->cells->PutWorkSheetComment($name, $sheetName="Sheet1", $cellName="A1", $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPutWorkSheetHyperlink()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PutWorkSheetHyperlink($name, $sheetName="Sheet1", $firstRow=1, $firstColumn=1, $totalRows=1, $totalColumns=1, $address="www.aspose.com", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPutWorkSheetValidation()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PutWorkSheetValidation($name, $sheetName="Sheet1", $range="A1:A5", $storage = null, $folder = null, $file = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPutWorkbookCreate()
    {
        $file = realpath(__DIR__ . '/../../../../..') . '/Data/test_convert_cell.xlsx';
        $result = $this->cells->PutWorkbookCreate($name="new_workbook.xlsx", $templateFile = null, $dataFile = null, $storage = null, $folder = null, $file);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPutWorksheetAddChart()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PutWorksheetAddChart($name, $sheetName="Sheet1", $chartType="bar", $upperLeftRow = 12, $upperLeftColumn = 12, $lowerRightRow = 20, $lowerRightColumn = 20, $area = null, $isVertical = null, $categoryData = null, $isAutoGetSerialName = null, $title = null, $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPutWorksheetAddPicture()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $file = realpath(__DIR__ . '/../../../../..') . '/Data/watermark.png';
        $result = $this->cells->PutWorksheetAddPicture($name, $sheetName="Sheet3", $upperLeftRow = 12, $upperLeftColumn = 12, $lowerRightRow = 20, $lowerRightColumn = 20, $picturePath = "watermark.png", $storage = null, $folder = null, $file);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPutWorksheetChartLegend()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PutWorksheetChartLegend($name, $sheetName="Sheet1", $chartIndex="0", $storage = null, $folder = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPutWorksheetChartTitle()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $body = array("Title"=>array("Text"=>"Sales Report"));
        $result = $this->cells->PutWorksheetChartTitle($name, $sheetName="Sheet1", $chartIndex="0", $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPutWorksheetFreezePanes()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $result = $this->cells->PutWorksheetFreezePanes($name, $sheetName="Sheet1", $row=1, $column=1, $freezedRows=5, $freezedColumns=5, $folder = null, $storage = null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPutWorksheetOleObject()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_cells.xlsx";
        Utils::uploadFile($name);

        $body = '{
                    "DisplayAsIcon": true,
                    "FileFormatType": "",
                    "ImageSourceFullName": "watermark.png",
                    "IsAutoSize": true,
                    "IsLink": true,
                    "ProgID": "",
                    "SourceFullName": "ole.docx",
                    "Name": "OLE",
                    "MsoDrawingType": "",
                    "AutoShapeType": "",
                    "Placement": "",
                    "UpperLeftRow": 20,
                    "Top": 20,
                    "UpperLeftColumn": 20,
                    "Left": 20,
                    "LowerRightRow": 10,
                    "Bottom": 10,
                    "LowerRightColumn": 10,
                    "Right": 20,
                    "Width": 200,
                    "Height": 100,
                    "X": 0,
                    "Y": 0,
                    "RotationAngle": 0,
                    "HtmlText": "",
                    "Text": "ole object",
                    "AlternativeText": "no alternative text",
                    "TextHorizontalAlignment": "",
                    "TextHorizontalOverflow": "",
                    "TextOrientationType": "string",
                    "TextVerticalAlignment": "string",
                    "TextVerticalOverflow": "string",
                    "IsGroup": true,
                    "IsHidden": true,
                    "IsLockAspectRatio": true,
                    "IsLocked": true,
                    "IsPrintable": true,
                    "IsTextWrapped": true,
                    "IsWordArt": true,
                    "LinkedCell": "string",
                    "ZOrderPosition": 0
                  }';
        $result = $this->cells->PutWorksheetOleObject($name, $sheetName="Sheet3", $upperLeftRow = null, $upperLeftColumn = null, $height = null, $width = null, $oleFile = null, $imageFile = null, $storage = null, $folder = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPutWorksheetPivotTable()
    {
        // Upload file to Aspose Cloud Storage
        $name = "test_convert_cell.xlsx";
        Utils::uploadFile($name);

        $body = '{
                    "Name": "MyData",
                    "SourceData": "A1",
                    "DestCellName": "A2",
                    "UseSameSource": true,
                    "PivotFieldRows": [
                      0
                    ],
                    "PivotFieldColumns": [
                      0
                    ],
                    "PivotFieldData": [
                      0
                    ]
                  }';
        $result = $this->cells->PutWorksheetPivotTable($name, $sheetName="Sheet1", $storage = null, $folder = null, $sourceData = null, $destCellName = null, $tableName = null, $useSameSource = null, $body);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testGetWorksheetAutoFilter()
    {
        // Upload file to Aspose Cloud Storage
        $name = "Book1.xlsx";
        Utils::uploadFile($name);

        $sheet_name = "Sheet1";

        $result = $this->cells->getWorksheetAutoFilter($name, $sheet_name, $folder=null);
        print_r($result);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPutWorksheetFilter()
    {
        // Upload file to Aspose Cloud Storage
        $name = "AutoFilter_temp.xlsx";
        //Utils::uploadFile($name);

        $sheet_name = "Sheet1";
        $range="A1:C13";
        $field_index=0;
        $criteria="2008";
        $refresh = true;

        $result = $this->cells->putWorksheetFilter($name, $sheet_name, $range, $field_index, $criteria, $match_blanks=null, $refresh, $folder=null);
        print_r($result);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testDeleteWorksheetFilter()
    {
        // Upload file to Aspose Cloud Storage
        $name = "Book1.xlsx";
        Utils::uploadFile($name);

        $sheet_name="Sheet1";
        $field_index=0;
        $criteria="Year";

        $result = $this->cells->deleteWorksheetFilter($name, $sheet_name, $field_index, $criteria, $folder=null);
        $this->assertEquals(200, $result->Code);
    }

    public function testPutWorksheetDateFilter()
    {
        // Upload file to Aspose Cloud Storage
        $name = "Book1.xlsx";
        Utils::uploadFile($name);

        $sheet_name = "Sheet1";
        $range="A1:B1";
        $field_index=0;
        $date_time_grouping_type="Year";
        $year=1920;
        $match_blanks=false;
        $refresh=true;

        $result = $this->cells->putWorksheetDateFilter($name, $sheet_name, $range, $field_index, $date_time_grouping_type, $year, $month=null, $day=null, $hour=null, $minute=null, $second=null, $match_blanks, $refresh, $folder=null);
        $this->assertEquals(200, $result->Code);
    }

    public function testDeleteWorksheetDateFilter()
    {
        // Upload file to Aspose Cloud Storage
        $name = "Book1.xlsx";
        Utils::uploadFile($name);

        $sheet_name="Sheet1";
        $field_index=0;
        $date_time_grouping_type="Year";
        $year=1920;

        $result = $this->cells->deleteWorksheetDateFilter($name, $sheet_name, $field_index, $date_time_grouping_type, $year, $month=null, $day=null, $hour=null, $minute=null, $second=null, $folder=null);
        $this->assertEquals(200, $result->Code);
    }

    public function testPutWorksheetCustomFilter()
    {
        // Upload file to Aspose Cloud Storage
        $name = "AutoFilter.xlsx";
        Utils::uploadFile($name);

        $sheet_name = "Sheet1";
        $range="A1:B13";
        $field_index=0;
        $operator_type1="GreaterOrEqual";
        $criteria1 = 2008;
        $is_and = true;
        $operator_type2 = "LessOrEqual";
        $criteria2 = 2011;
        $refresh = true;

        $result = $this->cells->putWorksheetCustomFilter($name, $sheet_name, $range, $field_index, $operator_type1, $criteria1, $is_and, $operator_type2, $criteria2, $match_blanks=null, $refresh, $folder=null);
        $this->assertEquals(200, $result->Code);
    }

    public function testPutWorksheetIconFilter()
    {
        // Upload file to Aspose Cloud Storage
        $name = "Book1.xlsx";
        Utils::uploadFile($name);

        $sheet_name = "Sheet1";
        $range="A1:B1";
        $field_index=0;
        $icon_set_type="ArrowsGray3";
        $icon_id = 1;
        $match_blanks = "false";

        $result = $this->cells->putWorksheetIconFilter($name, $sheet_name, $range, $field_index, $icon_set_type, $icon_id, $match_blanks, $refresh=null, $folder=null);
        $this->assertEquals(200, $result->Code);
    }

    public function testPutWorksheetDynamicFilter()
    {
        // Upload file to Aspose Cloud Storage
        $name = "Book1.xlsx";
        Utils::uploadFile($name);

        $sheet_name = "Sheet1";
        $range="A1:B1";
        $field_index=0;
        $dynamic_filter_type="BelowAverage";
        $match_blanks=true;

        $result = $this->cells->putWorksheetDynamicFilter($name, $sheet_name, $range, $field_index, $dynamic_filter_type, $match_blanks, $refresh=null, $folder=null);
        $this->assertEquals(200, $result->Code);
    }
    
    public function testPutWorksheetFilterTop10()
    {
        // Upload file to Aspose Cloud Storage
        $name = "Book1.xlsx";
        Utils::uploadFile($name);

        $sheet_name = "Sheet1";
        $range="A1:B1";
        $field_index=0;
        $is_top="true";
        $item_count = 1;
        $match_blanks = "true"; 
        $is_percent = "true";

        $result = $this->cells->putWorksheetFilterTop10($name, $sheet_name, $range, $field_index, $is_top, $item_count, $is_percent, $match_blanks, $refresh=null, $folder=null);
        $this->assertEquals(200, $result->Code);
    }

    public function testPutWorksheetColorFilter()
    {
        // Upload file to Aspose Cloud Storage
        $name = "Book1.xlsx";
        Utils::uploadFile($name);

        $sheet_name = "Sheet1";
        $range="A1:B1";
        $field_index=0;

        $color_filter = '{
                          "Pattern": "Solid",
                          "ForegroundColor": {
                            "Color": {
                              "A": "255",
                              "R": "0",
                              "G": "255",
                              "B": "255"
                            },
                            "ThemeColor": {
                              "ColorType": "Text2",
                              "Tint": "1"
                            },
                            "Type": "Automatic"
                          },
                          "BackgroundColor": {
                            "Color": {
                              "A": "255",
                              "R": "255",
                              "G": "0",
                              "B": "0"
                            },
                            "ThemeColor": {
                              "ColorType": "Text2",
                              "Tint": "1"
                            },
                            "Type": "Automatic"
                          }
                        }';

        $match_blanks=true;

        $result = $this->cells->putWorksheetColorFilter($name, $sheet_name, $range, $field_index, $color_filter, $match_blanks, $refresh=null, $folder=null);
        $this->assertEquals(200, $result->Code);
    }

    public function testPostWorksheetMatchBlanks()
    {
        // Upload file to Aspose Cloud Storage
        $name = "Book1.xlsx";
        Utils::uploadFile($name);

        $sheet_name = "Sheet1";
        $field_index=0;

        $result = $this->cells->postWorksheetMatchBlanks($name, $sheet_name, $field_index, $folder=null);
        $this->assertEquals(200, $result->Code);
    }

    public function testPostWorksheetMatchNonBlanks()
    {
        // Upload file to Aspose Cloud Storage
        $name = "Book1.xlsx";
        Utils::uploadFile($name);

        $sheet_name = "Sheet1";
        $field_index=0;

        $result = $this->cells->postWorksheetMatchNonBlanks($name, $sheet_name, $field_index, $folder=null);
        $this->assertEquals(200, $result->Code);
    }

    public function testPostWorksheetAutoFilterRefresh()
    {
        // Upload file to Aspose Cloud Storage
        $name = "AutoFilter_temp.xlsx";
        //Utils::uploadFile($name);

        $sheet_name = "Sheet1";

        $result = $this->cells->postWorksheetAutoFilterRefresh($name, $sheet_name, $folder=null);
        $this->assertEquals(200, $result->Code);
    }

    public function testPostCellTextFormatting()
    {
        // Upload file to Aspose Cloud Storage
        $name = "Book1.xlsx";
        Utils::uploadFile($name);
        $sheetName = "Sheet1";
        $cellName="A1";
        $value="121211212112";
        $type="string";

        // Set cell value
        $result = $this->cells->PostWorksheetCellSetValue($name, $sheetName, $cellName, $value, $type, $formula = null, $storage = null, $folder = null);

        $options = '{
          "FontSetting": [
            {
              "Font": {
                "IsBold": "true",
                "Size": "24"
              },
              "Length": "5",
              "StartIndex": "0"
            },
            {
              "Font": {
                "IsItalic": "true",
                "Size": "15"
              },
              "Length": "4",
              "StartIndex": "5"
            }
          ]
        }';        

        $result = $this->cells->postCellTextFormatting($name, $sheetName, $cellName, $options, $folder=null);
        $this->assertEquals(200, $result->Code);
    }

    public function testPostSortTableData()
    {
        // Upload file to Aspose Cloud Storage
        $name = "Book1.xlsx";
        Utils::uploadFile($name);

        $sheet_name = "sheet7";
        $list_object_index = 1;

        $data_sorter = '{
          "CaseSensitive": "true",
          "KeyList": {
            "SortKey": {
              "Key": "1",
              "SortOrder": "Ascending"
            }
          }
        }';

        $result = $this->cells->postSortTableData($name, $sheet_name, $list_object_index, $data_sorter, $folder=null);
        $this->assertEquals(200, $result->Code);
    }

    public function testPostCalulateCellFormula()
    {
        // Upload file to Aspose Cloud Storage
        $name = "Book1.xlsx";
        Utils::uploadFile($name);
        $sheetName = "Sheet1";
        $cellName="A1";
        $formula = "NOW()";

        // Set cell value
        $result = $this->cells->PostWorksheetCellSetValue($name, $sheetName, $cellName, $value = null, $type = null, $formula, $storage = null, $folder = null);

        $options = '{
            "CalcStackSize": "1"
        }';

        $result = $this->cells->postCalulateCellFormula($name, $sheetName, $cellName, $options, $folder=null);
        $this->assertEquals(200, $result->Code);
    }

    public function testCellsChartsPostWorksheetChart()
    {
        // Upload file to Aspose Cloud Storage
        $name = "Book1.xlsx";
        Utils::uploadFile($name);
        $sheet_name = "sheet4";
        $chart_index= 1;

        $chart = '{
                    "Type": "line"
                  }';

        $result = $this->cells->cellsChartsPostWorksheetChart($name, $sheet_name, $chart_index, $chart, $storage=null, $folder=null);
        $this->assertEquals(200, $result->Code);
    }
}    